<?php

/**
 * GRUPO 3:
 * Patron Decorator
 *
 * Integrantes:
 * Burbano Pozo Daniel Alexander
 * Simaliza Chimbo Kléver Hernán
 * Manuel Vladimir Gómez Placencia
 * Castillo Jumbo José Darwin
 * Párraga Vera Carlos Luis
 */

interface IFiltro {
	function aplicar(array $db): array;
}

class Filtro implements IFiltro {

	function aplicar(array $db): array {
		return $db;
	}
}

class FiltroDecorator implements IFiltro {
	var IFiltro $decorado;

	public function __construct(IFiltro $decorado) {
		$this->decorado = $decorado;
	}

	function aplicar(array $db): array {
		return $this->decorado->aplicar($db);
	}
}

class FiltroRangoPrecio extends FiltroDecorator {

	var $min;
	var $max;

	function setRango($min, $max) {
		$this->min = $min;
		$this->max = $max;
		return $this;
	}

	function aplicar(array $db): array {
		$db = parent::aplicar($db);
		$db[] = "Rango: \$$this->min - \$$this->max";
		return $db;
	}

}

class FiltroUbicacion extends FiltroDecorator {

	var $latitud;
	var $longitud;

	function setUbicacion($lat, $lon) {
		$this->latitud = $lat;
		$this->longitud = $lon;
		return $this;
	}

	function aplicar(array $db): array {
		$db = parent::aplicar($db);
		$db[] = "Ubicacion geografica: (Lat: $this->latitud, Lon $this->longitud)";
		return $db;
	}

}

class FiltroCategoriaProducto extends FiltroDecorator {

	var $categoria;

	function aplicar(array $db): array {
		$db = parent::aplicar($db);
		$db[] = "Categoria: '$this->categoria'";
		return $db;
	}


}

// cliente

// 'base de datos'
$db = [];

$filtro = new Filtro();

$ubicacion = new FiltroUbicacion($filtro);
$ubicacion->setUbicacion(0.34, 0.12);

$rango = new FiltroRangoPrecio($ubicacion);
$rango->setRango(100, 500);

$filtroCategoria = new FiltroCategoriaProducto($rango);
$filtroCategoria->categoria = 'electrodomesticos';

$res = $filtroCategoria->aplicar($db);
echo "Filtros:\n";
print_r($res);

// SALIDA DEL EJEMPLO:
/*
Filtros:
Array
(
    [0] => Ubicacion geografica: (Lat: 0.34, Lon 0.12)
    [1] => Rango: $100 - $500
    [2] => Categoria: 'electrodomesticos'
)
*/


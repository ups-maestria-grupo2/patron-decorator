# Universidad Politécnica Salesiana
## Maestría de Software
## Patrones de diseño de software

**Grupo 3**

Integrantes: 

* Burbano Pozo Daniel Alexander
* Simaliza Chimbo Kléver Hernán
* Manuel Vladimir Gómez Placencia
* Castillo Jumbo José Darwin
* Párraga Vera Carlos Luis

## Ejemplo Decorator

Supongamos que un portal de ventas de productos se necesita hacer búsquedas, pero las búsquedas
dependerán de los filtros que decida el cliente en ese momento
•
Los filtros de la búsqueda serán:

- Ubicación
- Rango de precio
- Categoría de producto

PHP 8.0+

